package net.racing.race;

import net.racing.filework.FileWorker;
import net.racing.interfaces.Table;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Races implements Table, RaceRepository {

    private List<Race> races = new ArrayList<>();

    private static Races instance = new Races();

    public static Races getInstance() {
        return instance;
    }

    private Races() {}


    private Consumer<String> entityParser = rawRace -> {
        try {
            String[] piecesOfHourse = rawRace.split(" ");
            Race race = new Race();
            race.setRaceId(Integer.parseInt(piecesOfHourse[0]));
            race.setRaceStatus(piecesOfHourse[1].equals("1") ? RaceStatus.FINISHED : RaceStatus.ANNOUNCED);
            races.add(race);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    };

    private Function<Race, String> raceToString = race -> race.getRaceId() + " " +
            (race.getRaceStatus()==RaceStatus.ANNOUNCED ? "0" : "1") + " " +
            " \n";


    @Override
    public void save() {
        FileWorker.writeAllLinesToFile("data/races.txt", races.stream().map(raceToString).reduce((line, str)->str+line).orElse(""));
    }

    @Override
    public void initialize() {
        races = new ArrayList<>();
        FileWorker.getAllLinesFromFile("data/races.txt", entityParser);
    }

    @Override
    public Race include(Race race) {
        Optional<Integer> maxId = races.stream().map(Race::getRaceId).max(Comparator.comparing(id->id));
        race.setRaceId(maxId.orElse(0) + 1);
        race.setRaceStatus(RaceStatus.ANNOUNCED);
        races.add(race);
        return race;
    }

    @Override
    public void update(Race race) {

    }

    @Override
    public void deleteById(int id) {
        Race race = selectById(id);
        if (race != null) {
            races.remove(race);
        } else {
            System.out.println("Race not exist");
        }
    }

    @Override
    public Race selectById(int id) {
        return races.stream().filter(race -> race.getRaceId()==id).findFirst().orElse(null);
    }

    @Override
    public List<Race> selectAllAnnounced() {
        return races.stream().filter(race -> race.getRaceStatus().equals(RaceStatus.ANNOUNCED)).collect(Collectors.toList());
    }
}
