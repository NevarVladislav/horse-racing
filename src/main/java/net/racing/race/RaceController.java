package net.racing.race;

import net.racing.interfaces.CrudController;

import java.util.List;
import java.util.Scanner;

public class RaceController implements CrudController {

    private RaceRepository raceRepository = Races.getInstance();

    private Scanner scanner = new Scanner(System.in);

    @Override
    public void insert() {
        Race race = new Race();
        System.out.println(raceRepository.include(race).toString());
    }

    @Override
    public void select() {
        try {
            System.out.println("1. Показать выбранный по id\n2. Выбрать все актуальные");
            int variant = scanner.nextInt();
            switch (variant) {
                case 1:
                    System.out.println("Введите id");
                    int id = scanner.nextInt();
                    Race race = raceRepository.selectById(id);
                    System.out.println(race != null ? race.toString() : "Race not exist");
                    break;
                case 2:
                    List<Race> races = raceRepository.selectAllAnnounced();
                    for (Race race1 : races) {
                        System.out.println(race1.toString());
                    }
                    break;
            }
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {
        try {
            System.out.println("Введите id");
            int id = scanner.nextInt();
            raceRepository.deleteById(id);
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
