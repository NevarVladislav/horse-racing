package net.racing.race;

public enum RaceStatus {
    FINISHED,
    ANNOUNCED
}
