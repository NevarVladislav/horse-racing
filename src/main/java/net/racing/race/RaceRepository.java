package net.racing.race;

import java.util.List;

public interface RaceRepository {

    Race include(Race race);

    void update(Race race);

    void deleteById(int id);

    Race selectById(int id);

    List<Race> selectAllAnnounced();
}
