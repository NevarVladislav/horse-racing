package net.racing.racer;

import java.util.Objects;

public class Racer {

    private int racerId;

    private String nickName;

    private String name;

    private String secondName;

    private int bonus;

    private int staminaBonus;

    private int maxSpeedBonus;

    private int averageSpeedBonus;

    private int accelerateBonus;

    public int getRacerId() {
        return racerId;
    }

    public void setRacerId(int racerId) {
        this.racerId = racerId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    public int getStaminaBonus() {
        return staminaBonus;
    }

    public void setStaminaBonus(int staminaBonus) {
        this.staminaBonus = staminaBonus;
    }

    public int getMaxSpeedBonus() {
        return maxSpeedBonus;
    }

    public void setMaxSpeedBonus(int maxSpeedBonus) {
        this.maxSpeedBonus = maxSpeedBonus;
    }

    public int getAverageSpeedBonus() {
        return averageSpeedBonus;
    }

    public void setAverageSpeedBonus(int averageSpeedBonus) {
        this.averageSpeedBonus = averageSpeedBonus;
    }

    public int getAccelerateBonus() {
        return accelerateBonus;
    }

    public void setAccelerateBonus(int accelerateBonus) {
        this.accelerateBonus = accelerateBonus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Racer racer = (Racer) o;
        return racerId == racer.racerId &&
                bonus == racer.bonus &&
                staminaBonus == racer.staminaBonus &&
                maxSpeedBonus == racer.maxSpeedBonus &&
                averageSpeedBonus == racer.averageSpeedBonus &&
                accelerateBonus == racer.accelerateBonus &&
                Objects.equals(nickName, racer.nickName) &&
                Objects.equals(name, racer.name) &&
                Objects.equals(secondName, racer.secondName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(racerId, nickName, name, secondName, bonus, staminaBonus, maxSpeedBonus, averageSpeedBonus, accelerateBonus);
    }

    @Override
    public String toString() {
        return "Racer{" +
                "racerId=" + racerId +
                ", nickName='" + nickName + '\'' +
                ", name='" + name + '\'' +
                ", secondName='" + secondName + '\'' +
                ", bonus=" + bonus +
                ", staminaBonus=" + staminaBonus +
                ", maxSpeedBonus=" + maxSpeedBonus +
                ", averageSpeedBonus=" + averageSpeedBonus +
                ", accelerateBonus=" + accelerateBonus +
                '}';
    }
}
