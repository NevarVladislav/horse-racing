package net.racing.racer;

import net.racing.exceptions.DataWorkRuntimeException;
import net.racing.filework.FileWorker;
import net.racing.interfaces.Table;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class Racers implements Table, RacerRepository {

    private List<Racer> racers = new ArrayList<>();

    private static Racers instance = new Racers();

    public static Racers getInstance() {
        return instance;
    }

    private Racers() {}

    Consumer<String> entityParser = rawRace -> {
        try {
            String[] piecesOfRacer = rawRace.split(" ");
            Racer racer = new Racer();
            racer.setRacerId(Integer.parseInt(piecesOfRacer[0]));
            racer.setNickName(piecesOfRacer[1]);
            racer.setName(piecesOfRacer[2]);
            racer.setSecondName(piecesOfRacer[3]);
            racer.setBonus(Integer.parseInt(piecesOfRacer[4]));
            racer.setStaminaBonus(Integer.parseInt(piecesOfRacer[5]));
            racer.setMaxSpeedBonus(Integer.parseInt(piecesOfRacer[6]));
            racer.setAverageSpeedBonus(Integer.parseInt(piecesOfRacer[7]));
            racer.setAccelerateBonus(Integer.parseInt(piecesOfRacer[8]));
            racers.add(racer);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    };

    private Function<Racer, String> racerToString = racer -> racer.getRacerId() + " " +
            racer.getNickName() + " " +
            racer.getName() + " " +
            racer.getSecondName() + " " +
            racer.getBonus() + " " +
            racer.getStaminaBonus() + " " +
            racer.getMaxSpeedBonus() + " " +
            racer.getAverageSpeedBonus() + " " +
            racer.getAccelerateBonus() +
            " \n";

    @Override
    public void save() {
        FileWorker.writeAllLinesToFile("data/racers.txt", racers.stream().map(racerToString).reduce((line,str)->str+line).orElse(""));
    }

    @Override
    public void initialize() {
        racers = new ArrayList<>();
        FileWorker.getAllLinesFromFile("data/racers.txt", entityParser);
    }

    @Override
    public void insert(Racer entity) {
        if (racers.stream().anyMatch(racer -> racer.getNickName().toLowerCase().equals(entity.getNickName().toLowerCase()))) {
            throw new DataWorkRuntimeException("Racer with nick " + entity.getNickName() + " already exist");
        }
        Optional<Integer> maxId = racers.stream().map(Racer::getRacerId).max(Comparator.comparing(id->id));
        entity.setRacerId(maxId.orElse(0) + 1);
        racers.add(entity);
    }

    @Override
    public void update(Racer entity) {

    }

    @Override
    public Racer selectById(int id) {
        return racers.stream().filter(racer -> racer.getRacerId()==id).findFirst().orElse(null);
    }

    @Override
    public Racer selectByNick(String nick) {
        return racers.stream().filter(racer -> racer.getNickName().toLowerCase().equals(nick.toLowerCase())).findFirst().orElse(null);
    }

    @Override
    public List<Racer> selectAll() {
        return racers;
    }

    @Override
    public void deleteById(int id) {
        deleteFromList(selectById(id));
    }

    @Override
    public void deleteByNick(String nick) {
        deleteFromList(selectByNick(nick));
    }

    private void deleteFromList(Racer racer) {
        if (racer != null) {
            racers.remove(racer);
        } else {
            System.out.println("Racer not exist");
        }
    }
}
