package net.racing.racer;

import net.racing.interfaces.CrudController;

import java.util.List;
import java.util.Scanner;

public class RacerController implements CrudController {

    private RacerRepository racerRepository = Racers.getInstance();

    private Scanner scanner = new Scanner(System.in);

    @Override
    public void insert() {
        try {
            Racer racer = new Racer();
            System.out.println("Введите псевдоним наездника");
            racer.setNickName(scanner.next());
            System.out.println("Введите имя наездника");
            racer.setName(scanner.next());
            System.out.println("Введите фамилию наездника");
            racer.setSecondName(scanner.next());
            System.out.println("Введите общий бонус");
            racer.setBonus(scanner.nextInt());
            System.out.println("Введите бонус выносливости");
            racer.setAccelerateBonus(scanner.nextInt());
            System.out.println("Введите бонус к макс скорости");
            racer.setMaxSpeedBonus(scanner.nextInt());
            System.out.println("Введите бонус к средней скорости");
            racer.setAverageSpeedBonus(scanner.nextInt());
            System.out.println("Введите бонус ускорения");
            racer.setAccelerateBonus(scanner.nextInt());
            racerRepository.insert(racer);
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void select() {
        try {
            Racer racer;
            System.out.println("1. Select by id\n2. Select by nick\n3. Select all");
            int variant = scanner.nextInt();
            switch (variant) {
                case 1:
                    System.out.println("Введите id");
                    int id = scanner.nextInt();
                    racer = racerRepository.selectById(id);
                    System.out.println(racer!=null ? racer.toString() : "Racer not exist");
                    break;
                case 2:
                    System.out.println("Введите nick");
                    String nick = scanner.next();
                    racer = racerRepository.selectByNick(nick);
                    System.out.println(racer!=null ? racer.toString() : "Racer not exist");
                    break;
                case 3:
                    List<Racer> racers = racerRepository.selectAll();
                    racers.forEach(racer1 -> System.out.println(racer1.toString()));
                    break;
            }
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void update() {
        try {
            Racer racer = null;
            System.out.println("1. Select by id\n2. Select by nick");
            int variant = scanner.nextInt();
            switch (variant) {
                case 1:
                    System.out.println("Введите id");
                    int id = scanner.nextInt();
                    racer = racerRepository.selectById(id);
                    break;
                case 2:
                    System.out.println("Введите nick");
                    String nick = scanner.next();
                    racer = racerRepository.selectByNick(nick);
                    break;
            }
            if (racer != null) {
                System.out.println("Change parameter:\n1. Общий бонус\n2. Стамина\n3. Макс скорость\n4. Средняя скорость\n5. Ускорение");
                variant = scanner.nextInt();
                switch (variant) {
                    case 1:
                        System.out.println("Введите общий бонус");
                        racer.setBonus(scanner.nextInt());
                        break;
                    case 2:
                        System.out.println("Введите бонус выносливости");
                        racer.setStaminaBonus(scanner.nextInt());
                        break;
                    case 3:
                        System.out.println("Введите бонус макс скорости");
                        racer.setMaxSpeedBonus(scanner.nextInt());
                        break;
                    case 4:
                        System.out.println("Введите бонус средней скорости");
                        racer.setAverageSpeedBonus(scanner.nextInt());
                        break;
                    case 5:
                        System.out.println("Введите бонус ускорения");
                        racer.setAccelerateBonus(scanner.nextInt());
                        break;
                }
            } else {
                System.out.println("Racer not exist");
            }
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void delete() {
        try {
            System.out.println("1. Delete by id\n2. Delete by nick\n");
            int variant = scanner.nextInt();
            switch (variant) {
                case 1:
                    System.out.println("Введите id");
                    int id = scanner.nextInt();
                    racerRepository.deleteById(id);
                    break;
                case 2:
                    System.out.println("Введите nick");
                    String nick = scanner.next();
                    racerRepository.deleteByNick(nick);
                    break;
            }
        } catch (RuntimeException ex) {
            System.out.println(ex);
        }
    }
}
