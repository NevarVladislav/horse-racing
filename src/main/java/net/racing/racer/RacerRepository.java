package net.racing.racer;

import java.util.List;

public interface RacerRepository {

    void insert(Racer entity);

    void update(Racer entity);

    Racer selectById(int id);

    Racer selectByNick(String nick);

    List<Racer> selectAll();

    void deleteById(int id);

    void deleteByNick(String nick);
}
