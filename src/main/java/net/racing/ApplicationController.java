package net.racing;

import net.racing.client.ClientController;
import net.racing.horse.HorseController;
import net.racing.interfaces.CrudController;
import net.racing.race.RaceController;
import net.racing.racer.RacerController;
import net.racing.rate.RateController;
import net.racing.rate.RateService;
import net.racing.slot.SlotController;

import java.util.Scanner;

public class ApplicationController {

    private Scanner scanner = new Scanner(System.in);

    private RateService rateService = new RateService();

    private CrudController horseController = new HorseController();

    private CrudController racerController = new RacerController();

    private CrudController clientController = new ClientController();

    private CrudController slotController = new SlotController();

    private CrudController raceController = new RaceController();

    private CrudController rateController = new RateController();

    private final Database database;

    private boolean exit = false;

    public ApplicationController(Database database) {
        this.database = database;
    }

    public void start() {
        while (!exit) {
            firstStep();
        }
    }

    private void firstStep() {
        System.out.println("1. Create\n2. Select\n3. Update\n4. Delete\n5. RateByRaceId\n6. Save\n7. Exit");
        String variant = scanner.next();
        switch (variant) {
            case "1":
                doInsert();
                break;
            case "2":
                doSelect();
                break;
            case "3":
                doUpdate();
                break;
            case "4":
                doDelete();
                break;
            case "5":
                try {
                    System.out.println("Введите id гонки");
                    int id = scanner.nextInt();
                    String res = rateService.selectAllRatesByRaceId(id);
                    System.out.println(res);
                    break;
                } catch (RuntimeException ex) {
                    System.out.println(ex);
                }
                break;
            case "6":
                database.save();
                break;
            case "7":
                exit = true;
            default:
                break;
        }
    }

    private void doInsert() {
        System.out.println("1. Client\n2. Horse\n3. Race\n4. Racer\n5. Rate\n6. Slot");
        String variant = scanner.next();
        switch (variant) {
            case "1":
                clientController.insert();
                break;
            case "2":
                horseController.insert();
                break;
            case "3":
                raceController.insert();
                break;
            case "4":
                racerController.insert();
                break;
            case "5":
                rateController.insert();
                break;
            case "6":
                slotController.insert();
                break;
            default:
                break;
        }
    }

    private void doSelect() {
        System.out.println("1. Client\n2. Horse\n3. Race\n4. Racer\n5. Rate\n6. Slot");
        String variant = scanner.next();
        switch (variant) {
            case "1":
                clientController.select();
                break;
            case "2":
                horseController.select();
                break;
            case "3":
                raceController.select();
                break;
            case "4":
                racerController.select();
                break;
            case "5":
                rateController.select();
                break;
            case "6":
                slotController.select();
                break;
            default:
                break;
        }
    }

    private void doUpdate() {
        System.out.println("1. Client\n2. Horse\n3. Race\n4. Racer\n5. Rate\n6. Slot");
        String variant = scanner.next();
        switch (variant) {
            case "1":
                clientController.update();
                break;
            case "2":
                horseController.update();
                break;
            case "3":
                raceController.update();
                break;
            case "4":
                racerController.update();
                break;
            case "5":
                rateController.update();
                break;
            case "6":
                slotController.update();
                break;
            default:
                break;
        }
    }

    private void doDelete() {
        System.out.println("1. Client\n2. Horse\n3. Race\n4. Racer\n5. Rate\n6. Slot");
        String variant = scanner.next();
        switch (variant) {
            case "1":
                clientController.delete();
                break;
            case "2":
                horseController.delete();
                break;
            case "3":
                raceController.delete();
                break;
            case "4":
                racerController.delete();
                break;
            case "5":
                rateController.delete();
                break;
            case "6":
                slotController.delete();
                break;
            default:
                break;
        }
    }
}
