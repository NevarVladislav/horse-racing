package net.racing;

public class Application {

    public static void main(String[] args) {
        Database database = new Database();
        ApplicationController applicationController = new ApplicationController(database);
        applicationController.start();
    }
}
