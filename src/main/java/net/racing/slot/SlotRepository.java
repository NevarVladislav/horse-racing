package net.racing.slot;

import java.util.List;

public interface SlotRepository {

    Slot insert(Slot slot);

    Slot selectById(int id);

    List<Slot> selectAllByRaceId(int raceId);

    void update(Slot slot);

    void deleteById(int id);

    void deleteAllByRaceId(int raceId);
}
