package net.racing.slot;

import net.racing.filework.FileWorker;
import net.racing.interfaces.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Slots implements Table, SlotRepository {

    private List<Slot> slots = new ArrayList<>();

    private static Slots instance = new Slots();

    public static Slots getInstance() {
        return instance;
    }

    private Slots() {}

    Consumer<String> entityParser = rawRate -> {
        try {
            String[] piecesOfRate = rawRate.split(" ");
            Slot slot = new Slot();
            slot.setSlotId(Integer.parseInt(piecesOfRate[0]));
            slot.setHorseId(Integer.parseInt(piecesOfRate[1]));
            slot.setRacerId(Integer.parseInt(piecesOfRate[2]));
            slot.setRaceId(Integer.parseInt(piecesOfRate[3]));
            slot.setTotalRate(Integer.parseInt(piecesOfRate[4]));
            slots.add(slot);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    };

    private Function<Slot, String> slotToString = slot -> slot.getSlotId() + " " +
            slot.getHorseId() + " " +
            slot.getRacerId() + " " +
            slot.getTotalRate() + " " +
            " \n";

    @Override
    public void save() {
        FileWorker.writeAllLinesToFile("data/slots.txt", slots.stream().map(slotToString).reduce((line,str)->str+line).orElse(""));
    }

    @Override
    public void initialize() {
        slots = new ArrayList<>();
        FileWorker.getAllLinesFromFile("data/slots.txt", entityParser);
    }

    @Override
    public Slot insert(Slot slot) {
        return slot;
    }

    @Override
    public Slot selectById(int id) {
        return slots.stream().filter(slot -> slot.getSlotId()==id).findFirst().orElse(null);
    }

    @Override
    public List<Slot> selectAllByRaceId(int raceId) {
        return slots.stream().filter(slot -> slot.getRaceId()==raceId).collect(Collectors.toList());
    }

    @Override
    public void update(Slot slot) {

    }

    @Override
    public void deleteById(int id) {
        Slot slot = selectById(id);
        if (slot != null) {
            slots.remove(slot);
        } else {
            System.out.println("Slot not exist");
        }
    }

    @Override
    public void deleteAllByRaceId(int raceId) {
        slots.stream().filter(slot -> slot.getRaceId()==raceId).forEach(slot->deleteById(slot.getSlotId()));
    }
}
