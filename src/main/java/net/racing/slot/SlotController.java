package net.racing.slot;

import net.racing.interfaces.CrudController;

import java.util.List;
import java.util.Scanner;

public class SlotController implements CrudController {

    private SlotRepository slotDataWorker = Slots.getInstance();

    private Scanner scanner = new Scanner(System.in);

    @Override
    public void insert() {
        try {
            Slot slot = new Slot();
            System.out.println("Введите id лошади");
            slot.setHorseId(scanner.nextInt());
            System.out.println("Введите id наездника");
            slot.setRacerId(scanner.nextInt());
            System.out.println("Введите id гонки");
            slot.setRaceId(scanner.nextInt());
            System.out.println(slotDataWorker.insert(slot).toString());
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void select() {
        try {
            Slot slot;
            System.out.println("1. Select by id\n2. Select all");
            int variant = scanner.nextInt();
            switch (variant) {
                case 1:
                    System.out.println("Введите id");
                    int id = scanner.nextInt();
                    slot = slotDataWorker.selectById(id);
                    System.out.println(slot!=null ? slot.toString() : "Slot not exist");
                    break;
                case 2:
                    System.out.println("Введите race id");
                    int raceId = scanner.nextInt();
                    List<Slot> slots = slotDataWorker.selectAllByRaceId(raceId);
                    slots.forEach(slot1 -> System.out.println(slot1.toString()));
                    break;
            }
        } catch (RuntimeException ex) {
            System.out.println(ex);
        }
    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {
        try {
            Slot slot;
            System.out.println("1. Delete by id\n2. Delete all by race id");
            int variant = scanner.nextInt();
            switch (variant) {
                case 1:
                    System.out.println("Введите id");
                    int id = scanner.nextInt();
                    slotDataWorker.deleteById(id);
                    break;
                case 2:
                    System.out.println("Введите race id");
                    int raceId = scanner.nextInt();
                    slotDataWorker.deleteAllByRaceId(raceId);
                    break;
            }
        } catch (RuntimeException ex) {
            System.out.println(ex);
        }
    }
}
