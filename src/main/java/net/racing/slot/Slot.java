package net.racing.slot;

import java.util.Objects;

public class Slot {

    private int slotId;

    private int horseId;

    private int racerId;

    private int raceId;

    private int totalRate;

    public int getSlotId() {
        return slotId;
    }

    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }

    public int getHorseId() {
        return horseId;
    }

    public void setHorseId(int horseId) {
        this.horseId = horseId;
    }

    public int getRacerId() {
        return racerId;
    }

    public void setRacerId(int racerId) {
        this.racerId = racerId;
    }


    public int getTotalRate() {
        return totalRate;
    }

    public void setTotalRate(int totalRate) {
        this.totalRate = totalRate;
    }

    public int getRaceId() {
        return raceId;
    }

    public void setRaceId(int raceId) {
        this.raceId = raceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Slot slot = (Slot) o;
        return slotId == slot.slotId &&
                horseId == slot.horseId &&
                racerId == slot.racerId &&
                totalRate == slot.totalRate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(slotId, horseId, racerId, totalRate);
    }


    @Override
    public String toString() {
        return "Slot{" +
                "slotId=" + slotId +
                ", horseId=" + horseId +
                ", racerId=" + racerId +
                ", totalRate=" + totalRate +
                '}';
    }
}
