package net.racing.horse;

import java.util.List;

public interface HorseRepository {
    void insert(Horse entity);

    void update(Horse entity);

    Horse selectById(int id);

    Horse selectByName(String name);

    List<Horse> selectAll();

    void deleteById(int id);

    void deleteByName(String horseName);
}
