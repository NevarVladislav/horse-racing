package net.racing.horse;

import net.racing.exceptions.DataWorkRuntimeException;
import net.racing.interfaces.CrudController;

import java.util.Scanner;

public class HorseController implements CrudController {

    private Scanner scanner = new Scanner(System.in);

    private HorseRepository horseRepository = Horses.getInstance();

    @Override
    public void insert() {
        try {
            Horse horse = new Horse();
            System.out.println("Введите имя лошади");
            horse.setHorseName(scanner.next());
            System.out.println("Введите максимальную скорость лошади");
            horse.setMaxSpeed(scanner.nextInt());
            System.out.println("Введите среднюю скорость");
            horse.setAverageSpeed(scanner.nextInt());
            System.out.println("Введите выносливость");
            horse.setStamina(scanner.nextInt());
            System.out.println("Введиет ускорение лошади");
            horse.setAcceleration(scanner.nextInt());
            horse.setWinCount(0);
            horseRepository.insert(horse);
        } catch (DataWorkRuntimeException exception) {
            System.out.println(exception);
        }
    }

    @Override
    public void select() {
        try {
            System.out.println("Введите входной параметр");
            String param = scanner.next();
            Horse horse;
            switch (param.toLowerCase()) {
                case "id":
                    System.out.println("Введите id");
                    int id = scanner.nextInt();
                    horse = horseRepository.selectById(id);
                    if (horse != null) {
                        System.out.println(horse.toString());
                    } else {
                        System.out.println("Совпадений нет");
                    }
                    break;
                case "name":
                    System.out.println("Введите имя");
                    String name = scanner.next();
                    horse = horseRepository.selectByName(name);
                    if (horse != null) {
                        System.out.println(horse.toString());
                    } else {
                        System.out.println("Совпадений нет");
                    }
                    break;
                case "all":
                    horseRepository.selectAll().forEach(horse1 -> System.out.println(horse1.toString()));
                    break;
            }
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void update() {
        try {
            System.out.println("Введите входной параметр");
            String param = scanner.next();
            Horse horse = null;
            switch (param.toLowerCase()) {
                case "id":
                    System.out.println("Введите id");
                    int id = scanner.nextInt();
                    horse = horseRepository.selectById(id);
                    break;
                case "name":
                    System.out.println("Введите имя");
                    String name = scanner.next();
                    horse = horseRepository.selectByName(name);
                    break;
            }
            if (horse == null) {
                System.out.println("Совпадений нет");
            } else {
                System.out.println("Введите параметр для смены");
                param = scanner.next();
                int value;
                switch (param.toLowerCase()) {
                    case "maxspeed":
                        System.out.println("Введите новое значение");
                        value = scanner.nextInt();
                        horse.setMaxSpeed(value);
                        break;
                    case "averagespeed":
                        System.out.println("Введите новое значение");
                        value = scanner.nextInt();
                        horse.setAverageSpeed(value);
                        break;
                    case "acceleration":
                        System.out.println("Введите новое значение");
                        value = scanner.nextInt();
                        horse.setAcceleration(value);
                        break;
                    case "stamina":
                        System.out.println("Введите новое значение");
                        value = scanner.nextInt();
                        horse.setStamina(value);
                        break;
                }
            }
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void delete() {
        try {
            System.out.println("Введите входной параметр");
            String param = scanner.next();
            Horse horse;
            switch (param.toLowerCase()) {
                case "id":
                    System.out.println("Введите id");
                    int id = scanner.nextInt();
                    horseRepository.deleteById(id);
                    break;
                case "name":
                    System.out.println("Введите имя");
                    String name = scanner.next();
                    horseRepository.deleteByName(name);
                    break;
            }
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
