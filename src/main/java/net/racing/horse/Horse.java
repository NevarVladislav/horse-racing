package net.racing.horse;

import java.util.Objects;

public class Horse {

    private int horseId;

    private String horseName;

    private int maxSpeed;

    private int averageSpeed;

    private int stamina;

    private int acceleration;

    private int winCount;

    public int getHorseId() {
        return horseId;
    }

    public void setHorseId(int horseId) {
        this.horseId = horseId;
    }

    public String getHorseName() {
        return horseName;
    }

    public void setHorseName(String horseName) {
        this.horseName = horseName;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(int averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public int getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(int acceleration) {
        this.acceleration = acceleration;
    }

    public int getWinCount() {
        return winCount;
    }

    public void setWinCount(int winCount) {
        this.winCount = winCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Horse horse = (Horse) o;
        return horseId == horse.horseId &&
                maxSpeed == horse.maxSpeed &&
                averageSpeed == horse.averageSpeed &&
                stamina == horse.stamina &&
                acceleration == horse.acceleration &&
                winCount == horse.winCount &&
                Objects.equals(horseName, horse.horseName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(horseId, horseName, maxSpeed, averageSpeed, stamina, acceleration, winCount);
    }

    @Override
    public String toString() {
        return "Horse{" +
                "horseId=" + horseId +
                ", horseName='" + horseName + '\'' +
                ", maxSpeed=" + maxSpeed +
                ", averageSpeed=" + averageSpeed +
                ", stamina=" + stamina +
                ", acceleration=" + acceleration +
                ", winCount=" + winCount +
                '}';
    }
}
