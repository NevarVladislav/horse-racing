package net.racing.horse;

import net.racing.exceptions.DataWorkRuntimeException;
import net.racing.filework.FileWorker;
import net.racing.interfaces.Table;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class Horses implements Table, HorseRepository {

    private List<Horse> horses = new ArrayList<>();

    private static Horses instance = new Horses();

    public static Horses getInstance() {
        return instance;
    }

    private Horses() {
    }

    private Consumer<String> entityParser = rawHorse -> {
            try {
                String[] piecesOfHourse = rawHorse.split(" ");
                Horse horse = new Horse();
                horse.setHorseId(Integer.parseInt(piecesOfHourse[0]));
                horse.setHorseName(piecesOfHourse[1]);
                horse.setMaxSpeed(Integer.parseInt(piecesOfHourse[2]));
                horse.setAverageSpeed(Integer.parseInt(piecesOfHourse[3]));
                horse.setStamina(Integer.parseInt(piecesOfHourse[4]));
                horse.setAcceleration(Integer.parseInt(piecesOfHourse[5]));
                horse.setWinCount(Integer.parseInt(piecesOfHourse[6]));
                horses.add(horse);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
    };

    private Function<Horse, String> horseToString = horse -> horse.getHorseId() + " " +
            horse.getHorseName() + " " +
            horse.getMaxSpeed() + " " +
            horse.getAverageSpeed() + " " +
            horse.getStamina() + " " +
            horse.getAcceleration() + " " +
            horse.getWinCount() +
            " \n";


    @Override
    public void save() {
        FileWorker.writeAllLinesToFile("data/horses.txt", horses.stream().map(horseToString).reduce((line, str)->str+line).orElse(""));
    }

    @Override
    public void initialize() {
        horses = new ArrayList<>();
        FileWorker.getAllLinesFromFile("data/horses.txt", entityParser);
    }

    @Override
    public void insert(Horse entity) {
        horses.forEach(horse -> {
            if (horse.getHorseName().toUpperCase().equals(entity.getHorseName().toUpperCase())) {
                throw new DataWorkRuntimeException("Horse with name " + entity.getHorseName() + "already exist");
            }
        });
        Optional<Horse> horseWithMaxId =horses.stream().max(Comparator.comparing(horse -> ((Integer) horse.getHorseId())));
        if (horseWithMaxId.isPresent()) {
            entity.setHorseId(horseWithMaxId.get().getHorseId()+1);
        } else {
            entity.setHorseId(1);
        }
        horses.add(entity);
    }

    @Override
    public void update(Horse entity) {

    }

    @Override
    public Horse selectById(int id) {
        return horses.stream().filter(horse -> horse.getHorseId()==id).findFirst().get();
    }

    @Override
    public Horse selectByName(String horseName) {
        return horses.stream().filter(horse -> horse.getHorseName().equals(horseName)).findFirst().get();
    }

    @Override
    public List<Horse> selectAll() {
        return horses;
    }

    @Override
    public void deleteById(int id) {
        horses.remove(selectById(id));
    }

    @Override
    public void deleteByName(String horseName) {
        horses.remove(selectByName(horseName));
    }
}
