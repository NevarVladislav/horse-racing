package net.racing.interfaces;

public interface Table {

    void save();

    void initialize();
}
