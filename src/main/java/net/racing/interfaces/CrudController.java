package net.racing.interfaces;

public interface CrudController {

    void insert();

    void select();

    void update();

    void delete();
}
