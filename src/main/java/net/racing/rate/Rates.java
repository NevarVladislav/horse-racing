package net.racing.rate;

import net.racing.exceptions.DataWorkRuntimeException;
import net.racing.filework.FileWorker;
import net.racing.interfaces.Table;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Rates implements Table, RateRepository {

    private List<Rate> rates = new ArrayList<>();

    private static Rates inscance = new Rates();

    public static Rates getInstance() {
        return inscance;
    }

    private Rates() {}

    private Consumer<String> entityParser = rawRate -> {
        try {
            String[] piecesOfRate = rawRate.split(" ");
            Rate rate = new Rate();
            rate.setRateId(Integer.parseInt(piecesOfRate[0]));
            rate.setRaceId(Integer.parseInt(piecesOfRate[1]));
            rate.setUserId(Integer.parseInt(piecesOfRate[2]));
            rate.setSlotId(Integer.parseInt(piecesOfRate[3]));
            rate.setRate(Integer.parseInt(piecesOfRate[4]));
            rates.add(rate);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    };

    private Function<Rate, String> rateToString = horse -> horse.getRateId() + " " +
            horse.getRaceId() + " " +
            horse.getUserId() + " " +
            horse.getSlotId() + " " +
            horse.getRate() + " " +
            " \n";

    @Override
    public void save() {
        FileWorker.writeAllLinesToFile("data/rates.txt", rates.stream().map(rateToString).reduce((line, str)->str+line).orElse(""));
    }

    @Override
    public void initialize() {
        rates = new ArrayList<>();
        FileWorker.getAllLinesFromFile("data/rates.txt", entityParser);
    }

    @Override
    public void insert(Rate entity) {
        Optional<Integer> maxId = rates.stream().map(Rate::getRateId).max(Comparator.comparing(id->id));
        entity.setRateId(maxId.orElse(0) + 1);
        rates.add(entity);
    }

    @Override
    public void update(Rate entity) {
        Rate rate = selectById(entity.getRateId());
        if (rate != null) {
            rates.remove(rate);
            rates.add(rate);
        } else {
            throw new DataWorkRuntimeException("Rate with this id not exist");
        }
    }

    @Override
    public Rate selectById(int id) {
        return rates.stream().filter(rate -> rate.getRateId()==id).findFirst().orElse(null);
    }

    @Override
    public List<Rate> selectAllByClient(int clientId) {
        return rates.stream().filter(rate -> rate.getUserId()==clientId).collect(Collectors.toList());
    }

    @Override
    public List<Rate> selectAllByRace(int raceId) {
        return rates.stream().filter(rate -> rate.getRaceId()==raceId).collect(Collectors.toList());
    }

    @Override
    public List<Rate> selectAllBySlot(int slotId) {
        return rates.stream().filter(rate -> rate.getSlotId()==slotId).collect(Collectors.toList());
    }

    @Override
    public void deleteById(int id) {
        Rate rate = selectById(id);
        if (rate != null) {
            rates.remove(rate);
        } else {
            throw new DataWorkRuntimeException("Rate with this id not exist");
        }
    }
}
