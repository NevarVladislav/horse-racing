package net.racing.rate;

import java.util.Objects;

public class Rate {

    private int rateId;

    private int raceId;

    private int userId;

    private int slotId;

    private int rate;

    public int getRateId() {
        return rateId;
    }

    public void setRateId(int rateId) {
        this.rateId = rateId;
    }

    public int getRaceId() {
        return raceId;
    }

    public void setRaceId(int raceId) {
        this.raceId = raceId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getSlotId() {
        return slotId;
    }

    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rate rate1 = (Rate) o;
        return rateId == rate1.rateId &&
                raceId == rate1.raceId &&
                userId == rate1.userId &&
                slotId == rate1.slotId &&
                rate == rate1.rate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rateId, raceId, userId, slotId, rate);
    }

    @Override
    public String toString() {
        return "Rate{" +
                "rateId=" + rateId +
                ", raceId=" + raceId +
                ", userId=" + userId +
                ", slotId=" + slotId +
                ", rate=" + rate +
                '}';
    }
}
