package net.racing.rate;

import java.util.List;

public interface RateRepository {

    void insert(Rate entity);

    void update(Rate entity);

    Rate selectById(int id);

    List<Rate> selectAllByClient(int clientId);

    List<Rate> selectAllByRace(int raceId);

    List<Rate> selectAllBySlot(int slotId);

    void deleteById(int id);
}
