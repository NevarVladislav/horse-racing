package net.racing.rate;

import net.racing.slot.Slot;
import net.racing.slot.Slots;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RateService {

    private RateRepository rateRepository = Rates.getInstance();

    private Slots slotService = Slots.getInstance();

    public void createRate(Rate rate) {

    }

    public void deleteRate(int rateId) {
        rateRepository.deleteById(rateId);
    }

    public void updateRate(Rate rate) {
        deleteRate(rate.getRateId());
        createRate(rate);
    }

    List<Rate> selectAllByClient(int clientId) {
        return rateRepository.selectAllByClient(clientId);
    }

    List<Rate> selectAllByRace(int raceId) {
        return rateRepository.selectAllByRace(raceId);
    }

    List<Rate> selectAllBySlot(int slotId) {
        return selectAllBySlot(slotId);
    }

    public String selectAllRatesByRaceId(int raceId) {
        List<Integer> slotIds = slotService.selectAllByRaceId(raceId)
                .stream()
                .map(Slot::getSlotId)
                .collect(Collectors.toList());
        Map<Integer, Integer> rates = new HashMap<>();
        slotIds.forEach(slotId->rates.put(slotId, 0));
        selectAllByRace(raceId).forEach(rate-> {
            Integer sum = rates.get(rate.getSlotId());
            sum += rate.getRate();
            rates.put(rate.getSlotId(), sum);
        });
        StringBuilder result = new StringBuilder();
        for (var slotId:slotIds) {
            result.append(slotId).append(": ").append(rates.get(slotId)).append("\n");
        }
        return result.toString();
    }

}
