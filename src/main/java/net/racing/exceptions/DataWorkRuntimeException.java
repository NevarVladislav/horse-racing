package net.racing.exceptions;

public class DataWorkRuntimeException extends RuntimeException {

    public DataWorkRuntimeException(String message) {
        super(message);
    }
}
