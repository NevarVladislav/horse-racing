package net.racing.client;

import net.racing.interfaces.CrudController;

import java.util.List;
import java.util.Scanner;

public class ClientController implements CrudController {

    private ClientRepository clientRepository = Clients.getInstance();

    private Scanner scanner = new Scanner(System.in);

    @Override
    public void insert() {
        try {
            Client client = new Client();
            System.out.println("Введите псевдоним клиента");
            client.setUsername(scanner.next());
            System.out.println("Введите имя клиента");
            client.setName(scanner.next());
            System.out.println("Введите фамилию клиента");
            client.setSecondName(scanner.next());
            clientRepository.insert(client);
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void select() {
        try {
            Client client;
            System.out.println("1. Select by id\n2. Select by username\n3. Select all");
            int variant = scanner.nextInt();
            switch (variant) {
                case 1:
                    System.out.println("Введите id");
                    int id = scanner.nextInt();
                    client = clientRepository.selectById(id);
                    System.out.println(client!=null ? client.toString() : "Client not exist");
                    break;
                case 2:
                    System.out.println("Введите nick");
                    String username = scanner.next();
                    client = clientRepository.selectByUsername(username);
                    System.out.println(client!=null ? client.toString() : "Client not exist");
                    break;
                case 3:
                    List<Client> clients = clientRepository.selectAll();
                    clients.forEach(client1 -> System.out.println(client1.toString()));
                    break;
            }
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {
        try {
            System.out.println("1. Delete by id\n2. Delete by nick\n");
            int variant = scanner.nextInt();
            switch (variant) {
                case 1:
                    System.out.println("Введите id");
                    int id = scanner.nextInt();
                    clientRepository.deleteById(id);
                    break;
                case 2:
                    System.out.println("Введите username");
                    String username = scanner.next();
                    clientRepository.deleteByUsername(username);
                    break;
            }
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
