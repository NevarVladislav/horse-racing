package net.racing.client;

import net.racing.exceptions.DataWorkRuntimeException;
import net.racing.filework.FileWorker;
import net.racing.interfaces.Table;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class Clients implements Table, ClientRepository {

    public static Clients getInstance() {
        return instance;
    }

    private static Clients instance = new Clients();

    private Clients(){}

    private List<Client> clients = new ArrayList<>();

    private Consumer<String> entityParser = rawClient -> {
        try {
            String[] piecesOfHourse = rawClient.split(" ");
            Client client = new Client();
            client.setId(Integer.parseInt(piecesOfHourse[0]));
            client.setUsername(piecesOfHourse[1]);
            client.setName(piecesOfHourse[2]);
            client.setSecondName(piecesOfHourse[3]);
            clients.add(client);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    };

    private Function<Client, String> clientToString = client -> client.getId() + " " +
            client.getUsername() + " " +
            client.getName() + " " +
            client.getSecondName() + " " +
            " \n";

    @Override
    public void save() {
        FileWorker.writeAllLinesToFile("data/clients.txt", clients.stream().map(clientToString).reduce((line, str)->str+line).orElse(""));
    }

    @Override
    public void initialize() {
        clients = new ArrayList<>();
        FileWorker.getAllLinesFromFile("data/clients.txt", entityParser);
    }

    @Override
    public void insert(Client entity) {
        if (clients.stream().anyMatch(client -> client.getUsername().toLowerCase().equals(entity.getUsername().toLowerCase()))) {
            throw new DataWorkRuntimeException("Client with username " + entity.getUsername() + " already exist");
        }
        Optional<Integer> maxId = clients.stream().map(Client::getId).max(Comparator.comparing(id->id));
        entity.setId(maxId.orElse(0) + 1);
        clients.add(entity);
    }

    @Override
    public void update(Client entity) {

    }

    @Override
    public Client selectById(int id) {
        return clients.stream().filter(client -> client.getId()==id).findFirst().orElse(null);
    }

    @Override
    public Client selectByUsername(String username) {
        return clients.stream().filter(client -> client.getUsername().toLowerCase().equals(username.toLowerCase())).findFirst().orElse(null);
    }

    @Override
    public List<Client> selectAll() {
        return clients;
    }

    @Override
    public void deleteById(int id) {
        deleteFromList(selectById(id));
    }

    @Override
    public void deleteByUsername(String username) {
        deleteFromList(selectByUsername(username));
    }

    private void deleteFromList(Client client) {
        if (client != null) {
            clients.remove(client);
        } else {
            System.out.println("Client not exist");
        }
    }
}
