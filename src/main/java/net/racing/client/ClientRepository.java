package net.racing.client;

import java.util.List;

public interface ClientRepository {

    void insert(Client entity);

    void update(Client entity);

    Client selectById(int id);

    Client selectByUsername(String username);

    List<Client> selectAll();

    void deleteById(int id);

    void deleteByUsername(String username);
}
