package net.racing;

import net.racing.client.Clients;
import net.racing.horse.Horses;
import net.racing.interfaces.Table;
import net.racing.race.Races;
import net.racing.racer.Racers;
import net.racing.rate.Rates;
import net.racing.slot.Slots;

import java.util.ArrayList;
import java.util.List;

public class Database {

    private List<Table> tables = new ArrayList<>();

    public void save() {
        tables.forEach(Table::save);
    }

    public void initialize() {
        tables.forEach(Table::initialize);
    }

    public Database() {
        tables.add(Races.getInstance());
        tables.add(Slots.getInstance());
        tables.add(Racers.getInstance());
        tables.add(Horses.getInstance());
        tables.add(Rates.getInstance());
        tables.add(Clients.getInstance());
        initialize();
    }
}
