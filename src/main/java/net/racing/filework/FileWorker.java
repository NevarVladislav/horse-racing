package net.racing.filework;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class FileWorker {

    public static void getAllLinesFromFile(String path, Consumer<String> parser) {
        try {
            ClassLoader classLoader = FileWorker.class.getClassLoader();
            URL resource = classLoader.getResource(path);
            Files.lines(Paths.get(resource.getFile().replaceFirst("/", "")), StandardCharsets.UTF_8).collect(Collectors.toList()).forEach(parser);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeAllLinesToFile(String path, String data) {
        try {
            ClassLoader classLoader = FileWorker.class.getClassLoader();
            URL resource = classLoader.getResource(path);
            try (FileWriter writer = new FileWriter(resource.getFile(), false)) {
                writer.write(data);
                writer.flush();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
